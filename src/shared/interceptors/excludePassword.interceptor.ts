import { Injectable, NestInterceptor, ExecutionContext, CallHandler } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable()
export class ExcludePasswordInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next
      .handle()
      .pipe(map(value => this.recursivelyStripPasswordValues(value)));
  }

  recursivelyStripPasswordValues(value: any): any {
    if (!value) {
      return;
    }
    if (Array.isArray(value)) {

      return value.map(this.recursivelyStripPasswordValues)
    }
    if (typeof value === 'object') {
      if ('password' in value) {

        let valueWithoutPassword = value.toObject();
        delete valueWithoutPassword['password']
        return valueWithoutPassword;

      }
      return this.createObjectFromEntries(
        Object.entries(value).map(([key, value]) =>
          [key, this.recursivelyStripPasswordValues(value)]
        ));
    }
    return value;
  }
  createObjectFromEntries(array) {
    return Object.assign({}, ...Array.from(array, ([key, value]) => ({ [key]: value })));
  }

}


