import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthModule } from '../auth/auth.module';
import { UsersModule } from '../users/users.module';
import { ConfigModule, ConfigService } from '@nestjs/config';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    MongooseModule.forRootAsync(
      {
        useFactory: async (configService: ConfigService) => ({
          uri: configService.get('MONGO_URI')
        }),
        inject: [ConfigService]
      }),
    UsersModule,
    AuthModule,
  ],
})
export class AppModule { }
