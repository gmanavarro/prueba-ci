import { Injectable, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { User, UserDocument } from './user.schema';

@Injectable()
export class UsersService {
  constructor(@InjectModel(User.name) private userModel: Model<UserDocument>) { }

  async getAppUsers(page: number, maxPerPage: number) {
    const entriesToSkip = maxPerPage * (page - 1);
    const totalEntries = await this.userModel.estimatedDocumentCount();
    let entries;
    try {
      entries = await this.userModel
        .find({ role: 'APP_USER' })
        .limit(maxPerPage * 1)
        .skip(entriesToSkip)
        .catch(err => {
          //console.log(err);
          throw new NotFoundException();
        });
    }
    catch (err) {
      console.log(err)
      throw new InternalServerErrorException('hola')
    }

    return {
      message: 'App users obtained successfully',
      page,
      maxPerPage,
      pages: Math.ceil(totalEntries / maxPerPage),
      entries,
    };
  }

  async getBackOfficeUsers(page: number, maxPerPage: number) {
    const entriesToSkip = maxPerPage * (page - 1);
    const totalEntries = await this.userModel.estimatedDocumentCount();
    const entries = await this.userModel
      .find({ role: 'ADMIN_USER' })
      .limit(maxPerPage * 1)
      .skip(entriesToSkip)
      .catch(err => {
        console.log(err);
        throw new NotFoundException();
      });

    return {
      message: 'Admin users obtained successfully',
      page,
      maxPerPage,
      pages: Math.ceil(totalEntries / maxPerPage),
      entries,
    };
  }

  async getUserById(id: string): Promise<User> {
    return this.userModel.findOne({ _id: id }).catch(err => {
      throw new NotFoundException('User not found');
    });
  }

  async getUserByEmail(email: string): Promise<User> {
    return this.userModel.findOne({ email: email }).catch(err => {
      throw new NotFoundException('User not found');
    });
  }

  // TO DO: Hay que devolver solo los usuarios que tienen el rol "ADMIN_USER"
  async getAdminUserById(id: string): Promise<User> {
    return this.userModel.findOne({ _id: id }).catch(err => {
      throw new NotFoundException('User not found');
    });
  }

  // TO DO: Hay que devolver solo los usuarios que tienen el rol "APP_USER"
  async getAppUserById(id: string): Promise<User> {
    return this.userModel.findOne({ _id: id }).catch(err => {
      throw new NotFoundException('User not found');
    });
  }

  async changeAppUserActiveStatus(user_id: string) {
    const user = await this.userModel.findOne({ _id: user_id });
    await this.userModel.findOneAndUpdate(
      { _id: user_id },
      { is_activated: !user.is_activated }
    );
  }

  async changeUserPassword(newPassword: string, email: string): Promise<User> {
    return this.userModel.updateOne({ email: email }, { password: newPassword })
      .catch(err => {
        throw new NotFoundException('user_not_found');
      })
  }
}
