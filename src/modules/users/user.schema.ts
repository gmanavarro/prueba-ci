import { Document } from 'mongoose';
import { } from 'class-validator';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

export type UserDocument = User & Document;

@Schema()
export class User {
  @Prop({ required: true })
  role: string;

  @Prop({ required: true, unique: true })
  email: string;

  @Prop({ required: true })
  password: string;

  @Prop({ required: true })
  name: string;

  @Prop()
  phone: number;

  @Prop()
  address: string;

  @Prop()
  state: string;

  @Prop()
  county: string;

  @Prop()
  city: string;

  @Prop()
  town: string;

  @Prop()
  zipcode: string;

  @Prop()
  is_confirmed: boolean

  @Prop()
  is_activated: boolean

  @Prop()
  image_url: string;

  @Prop()
  contact_information: string;

  @Prop()
  contact_phone: number;

  @Prop({ Date })
  created_at: Date;

  @Prop()
  updated_at: Date;
}
export const UserSchema = SchemaFactory.createForClass(User)
  .set('timestamps', { createdAt: 'created_at', updatedAt: 'updated_at' });