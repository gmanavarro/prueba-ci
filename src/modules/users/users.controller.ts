import {
  Controller,
  Query,
  Get,
  Param,
  UseGuards,
  UseInterceptors,
  Response,
  Request,
  Post,
  Body,
} from '@nestjs/common';
import { ExcludePasswordInterceptor } from '../../shared/interceptors/excludePassword.interceptor';
import { JwtAppAuthGuard } from '../../shared/guards/jwt-app-auth.guard';
import { JwtAdminAuthGuard } from '../../shared/guards/jwt-admin-auth.guard'
import { UsersService } from './users.service';
import { GetBackOfficeUserDTO } from './data_transfer_objects/get-bo-user.model';

@Controller()
export class UsersController {
  constructor(private readonly usersService: UsersService) { }

  @Get('app/users')
  @UseGuards(JwtAdminAuthGuard)
  @UseInterceptors(ExcludePasswordInterceptor)
  async getAppUsers(
    @Query('page') page: number,
    @Query('maxPerPage') maxPerPage: number,
  ) {
    return this.usersService.getAppUsers(page, maxPerPage);
  }

  @Get('app/user:id')
  @UseGuards(JwtAdminAuthGuard)
  @UseInterceptors(ExcludePasswordInterceptor)
  async getAppUserById(@Param('id') id) {
    return this.usersService.getAppUserById(id);
  }

  @Get('bo/users')
  @UseGuards(JwtAdminAuthGuard)
  @UseInterceptors(ExcludePasswordInterceptor)
  async getAdminUsers(
    @Query('page') page: number,
    @Query('maxPerPage') maxPerPage: number,
  ) {
    return this.usersService.getBackOfficeUsers(page, maxPerPage);
  }


  @Get('bo/user:id')
  @UseGuards(JwtAdminAuthGuard)
  @UseInterceptors(ExcludePasswordInterceptor)
  async getAdminUserById(
    @Param('id') id,
    @Request() request,
    @Response() response,
  ) {
    try {
      if (request.user.role !== 'APP_USER')
        return this.usersService.getAdminUserById(id);
      else return response.status(401).json({ message: 'User unauthorized' });
    } catch (err) {
      return response.status(500).json({ message: 'Server error, try again' });
    }
  }

  @Post('app/user/activate')
  @UseGuards(JwtAdminAuthGuard)
  async changeAppUserActiveStatus(
    @Body() requestBody,
    @Response() response,
    @Request() request,
  ) {
    try {
      if (request.user.role !== 'APP_USER') {
        await this.usersService.changeAppUserActiveStatus(
          requestBody['user_id'],
        );
        return response
          .status(200)
          .json({ message: 'App user activate status changed successfully' });
      } else {
        return response.status(401).json({ message: 'User unauthorized' });
      }
    } catch (err) {
      return response.status(500).json({ message: 'Server error, try again' });
    }
  }
}
