import { IsEmail, IsNumber, IsString } from 'class-validator';

export class RegisterAppUserDTO {
  @IsEmail()
  @IsString()
  email: string;

  @IsString()
  password: string;

  @IsString()
  name: string;

  @IsNumber()
  phone: number;

  @IsString()
  address: string;
}
