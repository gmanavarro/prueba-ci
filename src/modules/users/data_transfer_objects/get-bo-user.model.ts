import { IsEmail, IsNumber, IsString } from 'class-validator';

export class GetBackOfficeUserDTO {
  @IsEmail()
  @IsString()
  email: string;

  @IsString()
  password: string;

  @IsString()
  name: string;

  @IsNumber()
  phone: number;

  @IsString()
  state: string;

  @IsString()
  county: string;

  @IsString()
  city: string;

  @IsString()
  town: string;

  @IsNumber()
  zipcode: number;
}
