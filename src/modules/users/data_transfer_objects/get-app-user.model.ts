import { IsEmail, IsNumber, IsString } from 'class-validator';

export class GetAppUserDTO {
  @IsEmail()
  @IsString()
  email: string;

  @IsString()
  password: string;

  @IsString()
  name: string;

  @IsNumber()
  phone: number;

  @IsString()
  address: string;

  @IsString()
  contact_information: string;

  @IsNumber()
  contact_phone: number;
}
