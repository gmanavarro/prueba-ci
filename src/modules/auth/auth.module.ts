import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { AuthService } from './auth.service';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthController } from './auth.controller';
import { LocalStrategy } from '../../shared/strategies/local.strategy';
import { RefreshTokenSchema } from './refresh-token.schema';
import { UserSchema } from '../users/user.schema'
import { UsersModule } from '../users/users.module';
import { ConfirmationTokenSchema } from './confirmation-token.schema';
import { ConfigService } from '@nestjs/config';
import { SecurityService } from './services/security.service';
import { RegisterService } from './services/register.service';
import { MailService } from './services/mail.service';
import { JwtAdminStrategy } from '../../shared/strategies/jwt-admin.strategy';
import { JwtAppStrategy } from '../../shared/strategies/jwt-app.strategy';



@Module({
  imports: [
    /* JwtModule.registerAsync({
      useFactory: async (configService: ConfigService) => ({
      }),
      inject: [ConfigService]
    }
    ) */
    JwtModule.register({}),
    MongooseModule.forFeature([
      { name: 'User', schema: UserSchema },
      { name: 'RefreshToken', schema: RefreshTokenSchema },
      { name: 'ConfirmationToken', schema: ConfirmationTokenSchema }
    ]),
    UsersModule
  ],
  controllers: [AuthController],
  providers: [AuthService, MailService, SecurityService, RegisterService, LocalStrategy, JwtAppStrategy, JwtAdminStrategy],
  exports: [AuthService],
})
export class AuthModule { }
