import { Document } from 'mongoose';
import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';

// const schema = new Schema({
//   user: { type: Schema.Types.ObjectId, ref: 'User' },
//   token: String,
//   expires: Date,
//   // created: { type: Date, default: Date.now },
//   // createdByIp: String,
//   // revoked: Date,
//   // revokedByIp: String,
//   // replacedByToken: String
// });

export type RefreshTokenDocument = RefreshToken & Document;

@Schema()
export class RefreshToken {
  @Prop({ required: true, unique: true })
  token: string;

  @Prop({ required: true})
  email: string;

  @Prop({ Date })
  created_at: Date;

  @Prop()
  updated_at: Date;
}

export const RefreshTokenSchema = SchemaFactory.createForClass(
  RefreshToken,
).set('timestamps', { createdAt: 'created_at', updatedAt: 'updated_at' });
