import {
  Controller,
  Post,
  Body,
  Response,
  ValidationPipe,
  UseInterceptors,
  Param,
  Get,
  Redirect,
  UnauthorizedException,
  Request,
  UseGuards,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { ExcludePasswordInterceptor } from '../../shared/interceptors/excludePassword.interceptor';
import { DefaultSuccessResponse } from './responses.interface';
import { JwtAppAuthGuard } from '../../shared/guards/jwt-app-auth.guard';
import { JwtAdminAuthGuard } from '../../shared/guards/jwt-admin-auth.guard'
import { RegisterService } from './services/register.service';
import { LoginUserDTO } from '../users/data_transfer_objects/login-user.model';
import { RegisterAppUserDTO } from '../users/data_transfer_objects/register-app-user.model';
import { RegisterBackOfficeUserDTO } from '../users/data_transfer_objects/register-bo-user.model';

@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly registerService: RegisterService
  ) { }

  // REGISTER ROUTES ----------------------------------------------------------

  @Post('/app/register')
  @UseInterceptors(ExcludePasswordInterceptor)
  async registerAppUser(@Body(ValidationPipe) credentials: RegisterAppUserDTO) {
    return this.authService.registerAppUser(credentials);
  }

  @Post('/bo/register')
  @UseInterceptors(ExcludePasswordInterceptor)
  async registerBackOfficeUser(
    @Body(ValidationPipe) credentials: RegisterBackOfficeUserDTO,
  ) {
    return this.authService.registerBackOfficeUser(credentials);
  }

  @Get('/app/confirmation/:token')
  @Redirect()
  confirmAppUserRegistration(@Param('token') token: string) {
    this.registerService
      .confirmAppUserRegistration(token)
      .then(() => {
        console.log('redirect to confirmation successful view');
      })
      .catch(() => {
        console.log('redirect to confirmation failed view');
      });
  }

  // LOGIN ROUTES -------------------------------------------------------------

  @Post('/app/login')
  async loginAppUser(
    @Body(ValidationPipe) userToLogin: LoginUserDTO,
    @Response() response,
  ) {
    try {
      const validatedUser = await this.authService.validateUser(userToLogin);

      if (validatedUser == null)
        return response.status(400).json({
          message: 'Error: invalid user credentials',
        });

      if (validatedUser.role !== 'APP_USER') {
        return response.status(400).json({
          message: 'Error: the user is not an app user',
        });
      }

      if (validatedUser.isActivated === false) {
        return response.status(400).json({
          message: 'Error: the user is not activated'
        })
      }

      await this.authService.deleteRefreshTokenFrom(validatedUser);

      const tokens = await this.authService.createTokens(validatedUser, 'APP');
      return response.status(200).json({
        message: 'User logged in successfully',
        user: validatedUser,
        tokens,
      });
    } catch (err) {
      return response.status(500).json({
        message: 'Server error. Please try again',
      });
    }
  }

  @Post('/bo/login')
  async loginBackOfficeUser(
    @Body(ValidationPipe) userToLogin: LoginUserDTO,
    @Response() response,
  ) {
    try {
      const validatedUser = await this.authService.validateUser(userToLogin);
      if (validatedUser == null)
        return response.status(400).json({
          message: 'Error: invalid user credentials',
        });

      if (validatedUser.role === 'APP_USER') {
        return response.status(400).json({
          message: 'Error: the user is not an administrator',
        });
      }

      await this.authService.deleteRefreshTokenFrom(validatedUser);

      const tokens = await this.authService.createTokens(validatedUser, 'ADMIN');
      return response.status(200).json({
        message: 'User logged in successfully',
        user: validatedUser,
        tokens,
      });
    } catch (err) {
      return response.status(500).json({
        message: 'Server error. Please try again',
      });
    }
  }

  // REFRESH TOKEN ROUTE -----------------------------------------------------

  @Post('/bo/refresh-token')
  async refreshAdminUserToken(@Body() body, @Response() response) {
    const receivedRefreshToken = body['refresh_token'];

    if (await this.authService.verifyRefreshToken(receivedRefreshToken)) {
      const access_token = await this.authService.getNewAccessToken(
        receivedRefreshToken,
        'ADMIN'
      );

      return response.status(200).json({
        message: 'New access token created successfully',
        access_token,
      });
    }

    throw new UnauthorizedException('invalid_token');
  }

  @Post('/app/refresh-token')
  async refreshAppUserToken(@Body() body, @Response() response) {
    const receivedRefreshToken = body['refresh_token'];

    if (await this.authService.verifyRefreshToken(receivedRefreshToken)) {
      const access_token = await this.authService.getNewAccessToken(
        receivedRefreshToken,
        'APP'
      );

      return response.status(200).json({
        message: 'New access token created successfully',
        access_token,
      });
    }

    throw new UnauthorizedException('invalid_token');
  }
  // LOGOUT ROUTES ------------------------------------------------------------

  @Post('/bo/logout')
  @UseGuards(JwtAdminAuthGuard)
  async adminLogOut(@Request() request, @Response() response) {
    try {
      await this.authService.deleteRefreshTokenFrom(request.user);

      return response
        .status(200)
        .json({ message: 'User logged out successfully' });
    } catch (err) {
      return response
        .status(500)
        .json({ message: 'Server error. Please try again.' });
    }
  }

  @Post('/app/logout')
  @UseGuards(JwtAppAuthGuard)
  async appLogOut(@Request() request, @Response() response) {
    try {
      await this.authService.deleteRefreshTokenFrom(request.user);

      return response
        .status(200)
        .json({ message: 'User logged out successfully' });
    } catch (err) {
      return response
        .status(500)
        .json({ message: 'Server error. Please try again.' });
    }
  }
  // RESTORE PASSWORD ROUTES --------------------------------------------------

  @Post('/app/recover-password')
  async recoverPassword(
    @Body('email') email: string,
    @Response() response): Promise<DefaultSuccessResponse> {
    return response.status(200).json(await this.registerService.recoverPassword(email));
  }

  @Post('/app/reset-password')
  async resetPassword(
    @Body('password') newPassword: string,
    @Body('token') token: string,
    @Response() response): Promise<DefaultSuccessResponse> {
    return response.status(200).json(await this.registerService.resetPassword(newPassword, token));
  }
}
