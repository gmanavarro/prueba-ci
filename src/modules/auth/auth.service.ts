import {
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { UsersService } from '../users/users.service';
import { RefreshToken, RefreshTokenDocument } from './refresh-token.schema';
import { SecurityService } from './services/security.service';
import { MailService } from './services/mail.service';
import { RegisterService } from './services/register.service';


@Injectable()
export class AuthService {
  constructor(
    private readonly securityService: SecurityService,
    private readonly mailService: MailService,
    private readonly usersService: UsersService,
    private readonly registerService: RegisterService,
    @InjectModel(RefreshToken.name)
    private refreshTokenModel: Model<RefreshTokenDocument>,

  ) { }

  async createTokens(validatedUser, userType: string) {
    return await this.securityService.createTokens(validatedUser, userType);
  }

  registerAppUser(credentials) {
    return this.registerService.registerAppUser(credentials);
  }

  registerBackOfficeUser(credentials) {
    return this.registerService.registerBackOfficeUser(credentials);
  }

  async getNewAccessToken(refreshToken: string, userType: string) {
    await this.securityService.getNewAccessToken(refreshToken, userType);
  }

  async validateUser(userToValidate: any) {
    try {
      if (userToValidate && userToValidate.email && userToValidate.password) {
        const user = await this.usersService.getUserByEmail(
          userToValidate.email,
        );
        if (
          user &&
          (await this.securityService.comparePasswords(userToValidate.password, user.password))
        ) {
          const { password, ...result } = user['_doc'];
          return result;
        }
      }
    } catch (err) {
      console.log(err)
      return null;
    }
  }

  async verifyRefreshToken(refreshTokenToValidate: string) {
    const validatedRefreshToken = await this.refreshTokenModel.findOne({
      token: refreshTokenToValidate,
    });

    if (validatedRefreshToken === null) {
      return false;
    }

    return true;
  }

  async deleteRefreshTokenFrom(user: any) {
    await this.refreshTokenModel.findOneAndDelete({ email: user.email }).catch(
      err => {
        console.log('hola', err)
      }
    )
  }
}
