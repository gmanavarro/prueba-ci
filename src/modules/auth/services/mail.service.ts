import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
const sgMail = require('@sendgrid/mail')

@Injectable()
export class MailService {
  constructor(
    private readonly configService: ConfigService,
  ) { }
  async sendUserConfirmationMail(email: string, token: string) {
    const apiKey = this.configService.get('MAIL_API_KEY');
    const url = `http://localhost:3000/api/auth/app/confirmation/${token}`
    sgMail.setApiKey(apiKey);
    const mail = {
      to: email, // Receiver
      from: process.env.APP_MAIL_ACCOUNT, // Verified sender
      subject: 'Pet Connect - Confirm email',
      html: `Please, click this link to confirm your email: <a href="${url}">Confirm email</a>`,
    }
    sgMail
      .send(mail)
      .then(() => {
        console.log('Email sent');
      })
      .catch((error) => {
        console.error(error);
      })
  }

  sendRecoverPasswordMail(email: string, token: string) {
    const apiKey = this.configService.get('MAIL_API_KEY');
    const url = `${this.configService.get('RECOVER_PASSWORD_VIEW_URL')}${token}`
    sgMail.setApiKey(apiKey);
    const mail = {
      to: email, // Receiver
      from: process.env.APP_MAIL_ACCOUNT, // Verified sender
      subject: 'Pet Connect - Recover password',
      html: `Please, click this link to change your password: <a href="${url}">${token}</a>`,
    }
    sgMail
      .send(mail)
      .then(() => {
        console.log('Email sent');
      })
      .catch((error) => {
        console.error(error);
      })
  }
}
