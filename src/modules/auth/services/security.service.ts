import { Injectable, UnauthorizedException } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { JwtService } from "@nestjs/jwt";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { User, UserDocument } from '../../users/user.schema';
import { ConfirmationToken, ConfirmationTokenDocument } from "../confirmation-token.schema";
import { RefreshToken, RefreshTokenDocument } from "../refresh-token.schema";
const bcrypt = require('bcrypt');

@Injectable()
export class SecurityService {
  constructor(
    private readonly jwtService: JwtService,
    private readonly configService: ConfigService,
    @InjectModel(RefreshToken.name)
    private refreshTokenModel: Model<RefreshTokenDocument>,
    @InjectModel(ConfirmationToken.name)
    private confirmationTokenModel: Model<ConfirmationTokenDocument>,
    @InjectModel(User.name) private userModel: Model<UserDocument>,
  ) { }

  async comparePasswords(attempt: string, hashedPassword: string) {
    return await bcrypt.compare(attempt, hashedPassword);
  }

  async hashPassword(password: string) {
    return await bcrypt.hash(password, 12);

  }

  private async createToken(payload: any, expirationTime: string, secretKey: string) {
    return await this.jwtService.signAsync(payload, { expiresIn: expirationTime, secret: secretKey });
  }

  verifyToken(token) {
    return this.jwtService.verifyAsync(token)
      .catch(
        (err) => {
          console.log(err);
          if (err.name === 'TokenExpiredError') {
            throw new UnauthorizedException('token_expired');
          }
          else if (err.name === 'JsonWebTokenError') {
            throw new UnauthorizedException('invalid_token');
          }
        }
      );
  }

  async getConfirmationToken(token: string): Promise<ConfirmationToken> {
    return this.confirmationTokenModel.findOne({ token: token })
      .catch(err => {
        throw new UnauthorizedException('invalid_token')
      })
  }

  async createConfirmationToken(email: string) {
    const token = await this.createToken(
      { email },
      this.configService.get('CONFIRM_EMAIL_TOKEN_EXPIRATION'),
      this.configService.get('JWT_CONFIRMATION')
    )
    await new this.confirmationTokenModel({ email, token }).save();
    return token;
  }

  async createTokens(user: any, userType: string) {
    const payload = { email: user.email, sub: user.first_name };

    const refreshToken = await this.createToken(
      { email: user.email },
      this.configService.get('REFRESH_TOKEN_EXPIRATION'),
      this.configService.get(`JWT_REFRESH_${userType}`)
    );
    await new this.refreshTokenModel({
      email: user.email,
      token: refreshToken
    }).save()

    return {
      access_token: await this.createToken(
        payload,
        this.configService.get('ACCESS_TOKEN_EXPIRATION'),
        this.configService.get(`JWT_ACCESS_${userType}`)
      ),
      refresh_token: refreshToken
    }
  }

  async getNewAccessToken(refreshTokenString: string, userType: string) {
    const refreshToken = await this.refreshTokenModel.findOne({
      token: refreshTokenString,
    });

    const refreshTokenUser = await this.userModel.findOne({
      email: refreshToken.email,
    });

    return await this.createToken({
      email: refreshTokenUser.email,
      sub: refreshTokenUser.name,
    },
      this.configService.get('ACCESS_TOKEN_EXPIRATION'),
      this.configService.get(`JWT_ACCESS${userType}`)
    )
  }

}