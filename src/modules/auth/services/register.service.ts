import {
  ConflictException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User, UserDocument } from '../../users/user.schema';
import { MailService } from './mail.service';
import { SecurityService } from './security.service';
import { UsersService } from 'src/modules/users/users.service';
import { RegisterAppUserDTO } from '../../users/data_transfer_objects/register-app-user.model';
import { RegisterBackOfficeUserDTO } from '../../users/data_transfer_objects/register-bo-user.model';

@Injectable()
export class RegisterService {
  constructor(
    private readonly mailService: MailService,
    private readonly securityService: SecurityService,
    private readonly usersService: UsersService,
    @InjectModel(User.name) private userModel: Model<UserDocument>,
  ) {}

  async registerAppUser(credentials: RegisterAppUserDTO) {
    try {
      const token = await this.securityService.createConfirmationToken(
        credentials.email,
      );

      await this.mailService.sendUserConfirmationMail(credentials.email, token);

      const user = new this.userModel(credentials);
      user.password = await this.securityService.hashPassword(user.password);
      user.role = 'APP_USER';
      user.is_confirmed = false;
      user.is_activated = true;

      await user.save();

      return user;
    } catch (err) {
      if (err.name === 'MongoError' && err.code === 11000) {
        throw new ConflictException('email_taken');
      }
      console.log(err);
      throw new InternalServerErrorException('internal_error');
    }
  }

  async registerBackOfficeUser(credentials: RegisterBackOfficeUserDTO) {
    try {
      const user = new this.userModel(credentials);
      user.password = await this.securityService.hashPassword(user.password);
      user.role = 'ADMIN_USER';
      await user.save();
      return user;
    } catch (err) {
      if (err.name === 'MongoError' && err.code === 11000) {
        throw new ConflictException('email_taken');
      }
      console.log(err);
      throw new InternalServerErrorException('internal_error');
    }
  }

  async confirmAppUserRegistration(token: string) {
    try {
      const { email } = await this.securityService
        .verifyToken(token)
        .catch(err => {
          if (err.name === 'TokenExpiredError') {
            throw new UnauthorizedException('token_expired');
          }
          if (err.name === 'JsonWebTokenError') {
            throw new UnauthorizedException('invalid_token');
          }
        });
      await this.userModel.updateOne({ email: email }, { is_confirmed: true });
      console.log('confirmation successful');
    } catch (err) {
      throw err;
    }
  }

  async recoverPassword(email: string) {
    try {
      const user = await this.usersService.getUserByEmail(email);
      if (!user) {
        throw new NotFoundException('user_not_found');
      }
      const token = await this.securityService.createConfirmationToken(email);
      this.mailService.sendRecoverPasswordMail(email, token);

      return {
        message: 'Recover password mail sent',
        status: 200,
      };
    } catch (err) {
      if ((err.message = 'user_not_found')) {
        throw err;
      }
      throw new InternalServerErrorException('internal_error');
    }
  }

  async resetPassword(newPassword: string, token: string) {
    try {
      const { email } = await this.securityService.verifyToken(token);
      await this.securityService.getConfirmationToken(token);
      const hashedPassword = await this.securityService.hashPassword(
        newPassword,
      );
      this.usersService;
      this.userModel
        .updateOne({ email: email }, { password: hashedPassword })
        .catch(err => {
          console.log(err);
          throw new InternalServerErrorException('internal_error');
        });
      return {
        message: 'Password changed successfully',
        status: 200,
      };
    } catch (err) {
      throw err;
    }
  }
}
