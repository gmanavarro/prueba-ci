import { Document } from 'mongoose';
import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';

export type ConfirmationTokenDocument = ConfirmationToken & Document;

@Schema()
export class ConfirmationToken {
  @Prop({ required: true })
  email: string;

  @Prop({ required: true })
  token: string;

  @Prop({})
  created_at: Date;
}

export const ConfirmationTokenSchema = SchemaFactory.createForClass(
  ConfirmationToken,
).set('timestamps', { createdAt: 'created_at' });
