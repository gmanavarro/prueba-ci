export interface DefaultSuccessResponse {
  message: string,
  status: number,
}