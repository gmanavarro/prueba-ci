import { Test, TestingModule } from '@nestjs/testing';
import { BackOfficeUsersController } from '../src/interfaces/users-bo.controller';

describe('BackOfficeUsersController', () => {
  let controller: BackOfficeUsersController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BackOfficeUsersController],
    }).compile();

    controller = module.get<BackOfficeUsersController>(BackOfficeUsersController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
